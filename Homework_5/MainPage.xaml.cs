﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Collections.ObjectModel;

namespace Homework_5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
            var PinsList = new List<Pin>();
            var initialMapLocation = MapSpan.FromCenterAndRadius(new Position(33.131147, -117.159828), Distance.FromMiles(3));
            MMap.MoveToRegion(initialMapLocation);
            Pin pin = new Pin { Address = "328 S Twin Oaks Valley Rd, San Marcos, CA 92078", Position = new Position(33.129934, -117.165299), Label = "Players Sports Grill" };
            Pin pin2 = new Pin { Address = "583 Grand Ave, San Marcos, CA 92078", Position = new Position (33.137225, -117.177143), Label = "In-n-out" };
            Pin pin3 = new Pin { Address = "708 Center Dr, San Marcos, CA 92069", Position = new Position(33.133708, -117.122212), Label = "Panda Express" };
            Pin pin4 = new Pin { Address = "133 N Twin Oaks Valley Rd Suite #100, San Marcos, CA 92069", Position = new Position(33.139922, -117.162480), Label = "Subway" };
            PinsList.Add(pin);
            PinsList.Add(pin2);
            PinsList.Add(pin3);
            PinsList.Add(pin4);
            MMap.Pins.Add(pin);
            MMap.Pins.Add(pin2);
            MMap.Pins.Add(pin3);
            MMap.Pins.Add(pin4);
            PickerPins.ItemsSource = PinsList;
        }

        //function to modify type view into satellite
        void SatelliteView_Clicked(System.Object sender, System.EventArgs e)
        {
            if (MMap.MapType == Xamarin.Forms.Maps.MapType.Street)
                MMap.MapType = Xamarin.Forms.Maps.MapType.Satellite;
        }

        //function to modify type view into street
        void StreetView_Clicked(System.Object sender, System.EventArgs e)
        {
            if (MMap.MapType == Xamarin.Forms.Maps.MapType.Satellite)
                MMap.MapType = Xamarin.Forms.Maps.MapType.Street;
        }

        //On SelectedIndexChanged we move the position of the map to show the pin selected
        void PickerPins_SelectedIndexChanged(System.Object sender, System.EventArgs e)
        {
            Pin pin = (Pin)PickerPins.SelectedItem;
            switch (pin.Label)
            {
                case "Players Sports Grill":
                    MMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.129934, -117.165299), Distance.FromMiles(0.1)));
                    break;
                case "In-n-out":
                    MMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.137225, -117.177143), Distance.FromMiles(0.1)));
                    break;
                case "Subway":
                    MMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.139922, -117.162480), Distance.FromMiles(0.1)));
                    break;
                case "Panda Express":
                    MMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.133708, -117.122212), Distance.FromMiles(0.1)));
                    break;
                default:
                    MMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.131147, -117.159828), Distance.FromMiles(3)));
                    break;

            }
        }
    }
}
